function counterFactory() {
    let counter = 0;
    
    // `increment` should increment a counter variable in closure scope and return it.
    function increment() {
       return counter+=1;
    }
    
    // `decrement` should decrement the counter variable and return it.
    function decrement() {
       return counter-=1;
    }

    // Return an object that has two methods called `increment` and `decrement`.
    return { increment, decrement }; 
}

module.exports = counterFactory;