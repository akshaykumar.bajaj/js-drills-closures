let limitFunctionCallCount = require('../limitFunctionCallCount.js');

console.log("Data Checks:")
let invoke = limitFunctionCallCount();
invoke();
invoke = limitFunctionCallCount(console.log);
invoke();
invoke = limitFunctionCallCount(console.log,[2]);
invoke();

//Limit invokes of console.log to 3(passing arguments to invoke)
console.log('Test 1:');
invoke = limitFunctionCallCount(console.log, 3);

invoke('1');
invoke('2');
invoke('3');
invoke('4');

//Limit invokes of count.increment to 3(No arguements passed to invoke)
console.log('Test 2:');
let counter = require('../counterFactory.js')
let count = counter();

invoke = limitFunctionCallCount(count.increment, 3);

console.log(invoke());
console.log(invoke());
console.log(invoke());
console.log(invoke());
console.log(invoke());

//limit invokes of add function
console.log('Test 3:');
function add(a, b) { return a + b };
invoke = limitFunctionCallCount(add, 2);

console.log(invoke(1, 2));
console.log(invoke(5, 7));
console.log(invoke(8, 10));
console.log(invoke(3, 2));






