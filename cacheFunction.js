// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
    let cache = {};
    if (typeof cb !== 'function')
        return function () { };

    function invoke() {
        let current_arguments = Array.from(arguments);
        if (cache[current_arguments] === undefined) {
            console.log("Invoked");
            result = cb.apply(null, current_arguments);
            cache[current_arguments] = result;
            return result;
        }
        else {
            return cache[current_arguments];
        }
    }
    return invoke;
}

module.exports = cacheFunction;