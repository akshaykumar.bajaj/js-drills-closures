let cacheFunction = require('../cacheFunction.js');

function add(a, b) { return a + b };

//DataCheck
let invoke = cacheFunction();
console.log(invoke(3, 4));

//Test 1
invoke = cacheFunction(add);
console.log(invoke(5, 5));
console.log(invoke(3, 4));
console.log(invoke(4, 3));
console.log(invoke(5, 5));
